/*
 * Copyright © 2023 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <xf86drm.h>
#include <xf86drmMode.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <libweston/libweston.h>

#include "shared/helpers.h"
#include "weston-secure-display.h"
#include "secure-display-v1-server-protocol.h"

static struct secure_display_crtc_v1_interface secure_display_crtc_impl;
static struct secure_display_manager_v1_interface secure_display_manager_impl;

static struct secure_output *
secure_display_crtc_v1_from_resource(struct wl_resource *resource) {
	assert(wl_resource_instance_of(resource,
				       &secure_display_crtc_v1_interface,
				       &secure_display_crtc_impl));
	return wl_resource_get_user_data(resource);
}

static struct secure_display_manager *
secure_display_manager_v1_from_resource(struct wl_resource *resource) {
	assert(wl_resource_instance_of(resource,
				       &secure_display_manager_v1_interface,
				       &secure_display_manager_impl));
	return wl_resource_get_user_data(resource);
}

static void
get_crtc_crc(struct wl_client *client, struct wl_resource *resource)
{
	struct secure_output *sec_output = NULL;
	struct weston_output *output = NULL;
	struct get_crc_param crc;
	int ret = -1;

	sec_output = secure_display_crtc_v1_from_resource(resource);
	assert(sec_output);
	output = sec_output->w_output;

	if (output && output->get_secure_display_crc) {
		ret = output->get_secure_display_crc(output, &crc);
	}

	if (ret == 0) {
		printf("get_crtc_crc succeed.\n");
		secure_display_crtc_v1_send_rgb_crc(resource, crc.r_crc,
						    crc.g_crc, crc.b_crc,
						    crc.frame_count,
						    crc.x_start, crc.y_start,
					       	    crc.x_end, crc.y_end);
		secure_display_crtc_v1_send_done(resource);
	} else {
		printf("get_crtc_crc fail.\n");
	}

	return;
}

static void
set_crtc_roi(struct wl_client *client,
	     struct wl_resource *resource,
	     int32_t x_start,
	     int32_t y_start,
	     int32_t x_end,
	     int32_t y_end,
	     int32_t secure_display_enable)
{
	struct secure_output *sec_output = NULL;
	struct weston_output *output = NULL;
	struct set_roi_param roi;
	int ret = -1;

	sec_output = secure_display_crtc_v1_from_resource(resource);
	assert(sec_output);
	output = sec_output->w_output;

	if (output && output->set_secure_display_roi) {
		roi.x_start = x_start;
		roi.y_start = y_start;
		roi.x_end = x_end;
		roi.y_end = y_end;
		roi.secure_display_enable = secure_display_enable;

		ret = output->set_secure_display_roi(output, &roi);

		if (ret == 0) {
			printf("set_crtc_roi succeed.\n");
		} else {
			printf("set_crtc_roi fail.\n");
		}
	}

	return;
}

static void
destroy_display_resource(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

static void
destroy_crtc_resource(struct wl_client *client, struct wl_resource *resource)
{
	wl_resource_destroy(resource);
}

static void
destroy_secure_crtc(struct wl_resource *resource)
{
	struct secure_output *sec_output = secure_display_crtc_v1_from_resource(resource);

	wl_list_remove(&sec_output->link);

	free(sec_output);
}

static struct
secure_display_crtc_v1_interface secure_display_crtc_impl = {
	.destroy = destroy_crtc_resource,
	.set_roi = set_crtc_roi,
	.get_crc = get_crtc_crc,
};

static void
create_secure_display_output(struct wl_client *client,
			     struct wl_resource *resource,
			     uint32_t id,
			     struct wl_resource *output)
{
	struct weston_output *wes_output = NULL;
	struct wl_resource *secure_crtc_resource = NULL;
	struct secure_output *sec_output = NULL;

	wes_output = weston_head_from_resource(output)->output;
	assert(wes_output);

	sec_output = zalloc(sizeof(struct secure_output));
	if (!sec_output) {
		printf("create_secure_display_output: failed to create secure output\n");
		return;
	}

	secure_crtc_resource = wl_resource_create(client,
		&secure_display_crtc_v1_interface, 1, id);
	if (!secure_crtc_resource) {
		printf("create_secure_display_output: failed to allocate secure_crtc_resource\n");
		return;
	}

	wl_resource_set_implementation(secure_crtc_resource, &secure_display_crtc_impl,
				       sec_output, destroy_secure_crtc);

	sec_output->manager_backptr = secure_display_manager_v1_from_resource(resource);
	sec_output->w_output = wes_output;
	wl_list_insert(&sec_output->manager_backptr->secure_output, &sec_output->link);
}

static struct
secure_display_manager_v1_interface secure_display_manager_impl = {
	destroy_display_resource,
	create_secure_display_output,
};

static void
bind_secure_display(struct wl_client *client, void *data,
		    uint32_t version, uint32_t id)
{
	struct secure_display_manager *manager = data;
	struct wl_resource *secure_manager_resource;

	secure_manager_resource = wl_resource_create(client,
						     &secure_display_manager_v1_interface,
						     1, id);
	if (!secure_manager_resource) {
		wl_client_post_no_memory(client);
		return;
	}

	wl_list_init(&manager->secure_output);
	wl_resource_set_implementation(secure_manager_resource,
				       &secure_display_manager_impl,
				       NULL, NULL);

	wl_resource_set_user_data(secure_manager_resource, manager);
}

static void
secure_display_destroy_listener(struct wl_listener *listener, void *data)
{
	struct secure_display_manager *manager;

	manager = container_of(listener, struct secure_display_manager,
			       destroy_listener);
	wl_list_remove(&manager->destroy_listener.link);
	wl_list_remove(&manager->secure_output);

	manager->compositor->secure_display_manager = NULL;
	free(manager);
}

WL_EXPORT int
weston_compositor_enable_secure_display(struct weston_compositor *compositor)
{
	struct secure_display_manager *manager;

	manager = zalloc(sizeof(struct secure_display_manager));
	if (manager == NULL)
		return -1;

	manager->compositor = compositor;
	compositor->secure_display_manager = manager;

	if (wl_global_create(compositor->wl_display,
			     &secure_display_manager_v1_interface, 1, manager,
			     bind_secure_display) == NULL)
		return -1;

	manager->destroy_listener.notify = secure_display_destroy_listener;
	wl_signal_add(&compositor->destroy_signal, &manager->destroy_listener);

	return 0;
}
