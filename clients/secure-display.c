/*
 * Copyright © 2023 Advanced Micro Devices, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice (including the
 * next paragraph) shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "config.h"

#include <assert.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <libudev.h>
#include <sys/mman.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <signal.h>

#include <wayland-client.h>
#include <wayland-egl.h>
#include <libweston/zalloc.h>
#include "secure-display-v1-client-protocol.h"
#include "xdg-shell-client-protocol.h"
#include "xdg-output-unstable-v1-client-protocol.h"

#include <xf86drm.h>
#include <gbm.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "shared/platform.h"
#include "shared/xalloc.h"

#define VOID2U64(x) ((uint64_t)(unsigned long)(x))
static bool b_running = true;
static bool b_setroi = false;

struct secure_display {
	struct wl_display *display;
	struct secure_display_manager_v1 *secure_manager;
	struct wl_compositor *compositor;
	struct xdg_wm_base *wm_base;
	struct zxdg_output_manager_v1 *xdg_output_manager;

	struct {
		EGLDisplay dpy;
		EGLContext context;
		EGLConfig conf;
		PFNEGLCREATEIMAGEKHRPROC create_image;
		PFNEGLDESTROYIMAGEKHRPROC destroy_image;
		PFNEGLCREATESYNCKHRPROC create_sync;
		PFNEGLDESTROYSYNCKHRPROC destroy_sync;
		PFNEGLCLIENTWAITSYNCKHRPROC client_wait_sync;
		PFNEGLDUPNATIVEFENCEFDANDROIDPROC dup_native_fence_fd;
		PFNEGLWAITSYNCKHRPROC wait_sync;
	} egl;

	struct wl_list outputs;
	struct wl_list crtcs;
};

struct roi_region {
	int32_t x_start;
	int32_t y_start;
	int32_t x_end;
	int32_t y_end;
};

struct output {
	struct wl_list output_link;
	struct wl_output *output;
	struct {
		uint32_t flags;
		int32_t width, height, refresh;
	} current_mode;

	struct {
		int32_t x, y;
		int32_t width, height;
	} logical;

	char *name, *description;

	struct crtc *crtc;
	struct secure_display *display;
	struct zxdg_output_v1 *xdg_output;
};

struct crtc {
	struct secure_display_crtc_v1 *secure_crtc;
	int kms_out_fence_fd;
	struct roi_region roi;

	struct wl_list crtc_link;

	struct {
		uint32_t crc_r;
		uint32_t crc_g;
		uint32_t crc_b;
	} src_crc;

	bool b_roi;
	bool b_crc;

	struct output *output;
	struct secure_display *display;
};

struct window {
	struct secure_display *display;
	struct wl_surface *surface;
	struct xdg_surface *xdg_surface;
	struct xdg_toplevel *xdg_toplevel;
	struct wl_egl_window *native;
	EGLSurface egl_surface;
	int screen_width, screen_height;

	struct {
		GLuint program;
		GLuint pos;
		GLuint color;
		char c;
	} gles;

	struct output *output;
	struct wl_callback *callback;
	bool wait_for_configure;
	bool toplevel_fullscreen;
};

/*
 * these shader programes are inspired by weston application simple-dmabuf-feedback.c
 */
static const char *vert_shader_text =
	"attribute vec4 pos;\n"
	"attribute vec4 color;\n"
	"varying vec4 v_color;\n"
	"void main() {\n"
	"	gl_Position = pos;\n"
	"	v_color = color;\n"
	"}\n";

static const char *frag_shader_text =
	"precision mediump float;\n"
	"varying vec4 v_color;\n"
	"void main() {\n"
	"	gl_FragColor = v_color;\n"
	"}\n";

static GLuint
create_shader(const char *source, GLenum shader_type)
{
	GLuint shader;
	GLint status;

	shader = glCreateShader(shader_type);
	assert(shader != 0);

	glShaderSource(shader, 1, (const char **) &source, NULL);
	glCompileShader(shader);

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetShaderInfoLog(shader, 1000, &len, log);
		fprintf(stderr, "Error: compiling %s: %.*s\n",
			shader_type == GL_VERTEX_SHADER ? "vertex" : "fragment",
			len, log);
		return 0;
	}

	return shader;
}

static GLuint
create_and_link_program(GLuint vert, GLuint frag)
{
	GLint status;
	GLuint program = glCreateProgram();

	glAttachShader(program, vert);
	glAttachShader(program, frag);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status) {
		char log[1000];
		GLsizei len;
		glGetProgramInfoLog(program, 1000, &len, log);
		fprintf(stderr, "Error: linking:\n%.*s\n", len, log);
		return 0;
	}

	return program;
}

static void
init_gl(struct window *window)
{
	struct secure_display *display = window->display;
	EGLBoolean ret;
	GLuint vert;
	GLuint frag;

	window->native =
		wl_egl_window_create(window->surface,
				     window->screen_width,
				     window->screen_height);
	assert(window->native);

	window->egl_surface =
		weston_platform_create_egl_surface(display->egl.dpy,
					display->egl.conf,
					window->native, NULL);

	ret = eglMakeCurrent(display->egl.dpy,
			     window->egl_surface,
			     window->egl_surface,
			     display->egl.context);
	assert(ret == EGL_TRUE);

	vert = create_shader(vert_shader_text, GL_VERTEX_SHADER);
	if (vert == 0) {
		printf("failed to create vertex shader\n");
		return;
	}

	frag = create_shader(frag_shader_text, GL_FRAGMENT_SHADER);
	if (frag == 0) {
		printf("failed to create fragment shader\n");
		return;
	}

	window->gles.program = create_and_link_program(vert ,frag);
	if (window->gles.program == 0) {
		printf("failed to compile and link shader programes\n");
		return;
	}

	glDeleteShader(vert);
	glDeleteShader(frag);

	window->gles.pos =
		glGetAttribLocation(window->gles.program, "pos");
	window->gles.color =
		glGetAttribLocation(window->gles.program, "color");

	glUseProgram(window->gles.program);
}

/* roi region: screen coordinate with origin point locates at left-top
 * roi(x0,y0)
 *            +-------------+
 *            |             |
 *            |             |
 *            |             |
 *            |             |
 *            |             |
 *            +-------------+ roi(x1, y1)
 *
 * opengl vert: normalized device coordinate with origin point locates
 * at the center
 *
 *      vert1 +-------------+ vert3
 *            |             |
 *            |             |
 *            |             |
 *            |             |
 *            |             |
 *      vert0 +-------------+ vert2
 *
 * this function is to map screen coordinates of the roi region as rectangle
 * vertice, opengles then draws this rectangle.
 */
static void
vert_coord(struct roi_region *roi, int32_t screen_width, int32_t screen_height, GLfloat verts[4][2])
{
	int32_t x0, y0, x1, y1;

	x0 = roi->x_start;
	y0 = roi->y_start;
	x1 = roi->x_end;
	y1 = roi->x_end;

	//vert0
	verts[0][0] = (GLfloat)(x0*2.0f)/screen_width - 1.0f;
	verts[0][1] = 1.0f - (GLfloat)(y1*2.0f)/screen_height;
	//vert1
	verts[1][0] = (GLfloat)(x0*2.0f)/screen_width - 1.0f;
	verts[1][1] = 1.0f - (GLfloat)(y0*2.0f)/screen_height;
	//vert2
	verts[2][0] = (GLfloat)(x1*2.0f)/screen_width - 1.0f;
	verts[2][1] = 1.0f - (GLfloat)(y1*2.0f)/screen_height;
	//vert3
	verts[3][0] = (GLfloat)(x1*2.0f)/screen_width - 1.0f;
	verts[3][1] = 1.0f - (GLfloat)(y0*2.0f)/screen_height;
}

static const struct wl_callback_listener surface_frame_listener;

static void
redraw(void *data, struct wl_callback *callback, uint32_t time)
{
	struct window *window = data;
	struct output *output;
	struct roi_region *roi;
	struct secure_display *display;
	int32_t x0, y0, x1, y1;
	static GLfloat verts[4][2] = {0};
	static GLfloat colors[4][3] = {0};

	output = window->output;
	display = window->display;
	assert(output);
	assert(display);
	assert(output->crtc);

	x0 = output->crtc->roi.x_start;
	y0 = output->crtc->roi.y_start;
	x1 = output->crtc->roi.x_end;
	y1 = output->crtc->roi.y_end;
	roi = &(output->crtc->roi);

	printf("%s roi x0:%d, y0:%d, x1:%d, y1:%d\n", __func__, x0, y0, x1, y1);

	glViewport(0, 0, window->screen_width, window->screen_height);

	vert_coord(roi, window->screen_width, window->screen_height, verts);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	if (window->gles.c == 'R') {
		colors[0][0] = 1;
		colors[1][0] = 1;
		colors[2][0] = 1;
		colors[3][0] = 1;
	} else if (window->gles.c == 'G') {
		colors[0][1] = 1;
		colors[1][1] = 1;
		colors[2][1] = 1;
		colors[3][1] = 1;
	} else if (window->gles.c == 'B') {
		colors[0][2] = 1;
		colors[1][2] = 1;
		colors[2][2] = 1;
		colors[3][2] = 1;
	}

	glVertexAttribPointer(window->gles.pos, 2, GL_FLOAT, GL_FALSE, 0, verts);
	glVertexAttribPointer(window->gles.color, 3, GL_FLOAT, GL_FALSE, 0, colors);
	glEnableVertexAttribArray(window->gles.pos);
	glEnableVertexAttribArray(window->gles.color);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	if (callback)
		wl_callback_destroy(callback);

	window->callback = wl_surface_frame(window->surface);
	wl_callback_add_listener(window->callback, &surface_frame_listener, window);

	eglSwapBuffers(window->display->egl.dpy, window->egl_surface);

	wl_display_roundtrip(display->display);

	if (output->crtc->b_roi) {
		secure_display_crtc_v1_set_roi(output->crtc->secure_crtc,
					       x0, y0, x1, y1, 1);
		output->crtc->b_roi = false;
		output->crtc->b_crc = true;
	} else {
		secure_display_crtc_v1_get_crc(output->crtc->secure_crtc);
	}
}

static const struct wl_callback_listener surface_frame_listener = {
	.done = redraw,
};

static void
xdg_surface_configure(void *data, struct xdg_surface *surface,
			 uint32_t serial)
{
	struct window *window = data;

	xdg_surface_ack_configure(surface, serial);

	if (window->wait_for_configure) {
		window->wait_for_configure = false;
	}
}

static const struct xdg_surface_listener xdg_surface_listener = {
	.configure = xdg_surface_configure,
};

static void
xdg_toplevel_configure(void *data, struct xdg_toplevel *xdg_toplevel,
			int32_t width, int32_t height, struct wl_array *states)
{
	struct window *window = data;
	uint32_t *p;

	wl_array_for_each(p, states) {
		if (*p == XDG_TOPLEVEL_STATE_FULLSCREEN) {
			printf("%s, xdg toplevel is in full screen\n", __func__);
			window->toplevel_fullscreen = true;
			break;
		}
	}

	if (width > 0 && height > 0) {
		window->screen_width = width;
		window->screen_height = height;
	}
}

static void
xdg_toplevel_close(void *data, struct xdg_toplevel *xdg_toplevel)
{
	b_running = 0;
}

static const struct xdg_toplevel_listener xdg_toplevel_listener = {
	.configure = xdg_toplevel_configure,
	.close = xdg_toplevel_close,
};

static void
init_surface(struct window *window)
{
	struct secure_display *display = window->display;
	struct output *output = window->output;
	char app_id[50];

	window->surface = wl_compositor_create_surface(display->compositor);

	window->xdg_surface = xdg_wm_base_get_xdg_surface(display->wm_base,
							  window->surface);

	xdg_surface_add_listener(window->xdg_surface,
				 &xdg_surface_listener, window);

	window->xdg_toplevel =
		xdg_surface_get_toplevel(window->xdg_surface);
	xdg_toplevel_add_listener(window->xdg_toplevel,
				  &xdg_toplevel_listener, window);

	xdg_toplevel_set_title(window->xdg_toplevel, "secure-display");

	sprintf(app_id, "org.freedesktop.weston.secure-display-%s", output->name);
	xdg_toplevel_set_app_id(window->xdg_toplevel, app_id);
	xdg_toplevel_set_fullscreen(window->xdg_toplevel, NULL);

	window->wait_for_configure = true;
	wl_surface_commit(window->surface);

	wl_display_roundtrip(display->display);
}

static bool
init_egl(struct secure_display *display)
{
	static const EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	EGLint major, minor, ret;
	EGLint count = 0;
	const char *egl_extensions = NULL;
	const char *gl_extensions = NULL;

	EGLint config_attribs[] = {
		EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};

	display->egl.dpy =
		weston_platform_get_egl_display(EGL_PLATFORM_WAYLAND_KHR,
						display->display, NULL);
	if (display->egl.dpy == EGL_NO_DISPLAY) {
		printf("failed to create EGLDisplay\n");
		return false;
	}

	if (eglInitialize(display->egl.dpy, &major, &minor) == EGL_FALSE) {
		printf("failed to initialize EGLDisplay\n");
		return false;
	}

	if (eglBindAPI(EGL_OPENGL_ES_API) == EGL_FALSE) {
		printf("failed to bind OpenGLES API\n");
		return false;
	}

	egl_extensions = eglQueryString(display->egl.dpy, EGL_EXTENSIONS);
	if (egl_extensions != NULL) {
		printf("query egl extension succeed.\n");
	} else {
		printf("query egl extension fail.\n");
		return false;
	}

	ret = eglChooseConfig(display->egl.dpy, config_attribs,
			      &display->egl.conf, 1, &count);
	if (ret && count >= 1) {
		printf("choose egl config succeed.\n");
	} else {
		printf("choose egl config fail.\n");
		return false;
	}

	display->egl.context = eglCreateContext(display->egl.dpy,
						display->egl.conf,
						EGL_NO_CONTEXT,
						context_attribs);
	if (display->egl.context == EGL_NO_CONTEXT) {
		printf("failed to create EGLContext\n");
		return false;
	}

	eglMakeCurrent(display->egl.dpy,
		       EGL_NO_SURFACE, EGL_NO_SURFACE,
		       display->egl.context);

	gl_extensions = (const char *) glGetString(GL_EXTENSIONS);
	assert(gl_extensions != NULL);

	if (!weston_check_egl_extension(gl_extensions,
					"GL_OES_EGL_image")) {
		fprintf(stderr, "GL_OES_EGL_image not supported\n");
		goto fail;
	}

	display->egl.create_image =
		(void *) eglGetProcAddress("eglCreateImageKHR");
	assert(display->egl.create_image);

	display->egl.destroy_image =
		(void *) eglGetProcAddress("eglDestroyImageKHR");
	assert(display->egl.destroy_image);

	if (weston_check_egl_extension(egl_extensions, "EGL_KHR_fence_sync") &&
	    weston_check_egl_extension(egl_extensions,
				       "EGL_ANDROID_native_fence_sync")) {
		display->egl.create_sync =
			(void *) eglGetProcAddress("eglCreateSyncKHR");
		assert(display->egl.create_sync);

		display->egl.destroy_sync =
			(void *) eglGetProcAddress("eglDestroySyncKHR");
		assert(display->egl.destroy_sync);

		display->egl.client_wait_sync =
			(void *) eglGetProcAddress("eglClientWaitSyncKHR");
		assert(display->egl.client_wait_sync);

		display->egl.dup_native_fence_fd =
			(void *) eglGetProcAddress("eglDupNativeFenceFDANDROID");
		assert(display->egl.dup_native_fence_fd);
	}

	if (weston_check_egl_extension(egl_extensions,
				       "EGL_KHR_wait_sync")) {
		display->egl.wait_sync =
			(void *) eglGetProcAddress("eglWaitSyncKHR");
		assert(display->egl.wait_sync);
	}

	return true;

fail:
	if (display->egl.context != EGL_NO_CONTEXT)
		eglDestroyContext(display->egl.dpy, display->egl.context);

	return false;
}

static void
noop ()
{
}

static void
xdg_output_logical_position(void *data,
			    struct zxdg_output_v1 *output,
			    int32_t x, int32_t y)
{
	struct output *display_output = data;
	display_output->logical.x = x;
	display_output->logical.y = y;
}

static void
xdg_output_logical_size(void *data,
			struct zxdg_output_v1 *output,
			int32_t width, int32_t height)
{
	struct output *display_output = data;
	display_output->logical.width = width;
	display_output->logical.height = height;
}

static void
xdg_output_name(void *data,
		struct zxdg_output_v1 *output,
		const char *name)
{
	struct output *display_output = data;
	display_output->name = xstrdup(name);
}

static void
xdg_output_description(void *data,
		       struct zxdg_output_v1 *output,
		       const char *description)
{
	struct output *display_output = data;
	display_output->description = xstrdup(description);
}

static const struct zxdg_output_v1_listener xdg_output_listener = {
	.logical_position = xdg_output_logical_position,
	.logical_size = xdg_output_logical_size,
	.done = noop,
	.name = xdg_output_name,
	.description = xdg_output_description,
};

static void
add_output(struct secure_display *display,
	   struct wl_registry *wl_registry,
	   uint32_t name)
{
	struct output *display_output = zalloc(sizeof *display_output);
	if (!display_output) {
		printf("failed to alloc display_output\n");
		return;
	}

	display_output->output =
		wl_registry_bind(wl_registry, name, &wl_output_interface, 3);

	wl_list_insert(&display->outputs, &display_output->output_link);
	display_output->display = display;

	if (display->xdg_output_manager != NULL) {
		display_output->xdg_output =
			zxdg_output_manager_v1_get_xdg_output(
					display->xdg_output_manager, display_output->output);
		zxdg_output_v1_add_listener(display_output->xdg_output, &xdg_output_listener,
					display_output);
	}
}

static void
compare_rgb_crc(void *data,
		struct secure_display_crtc_v1 *secure_display_crtc_v1,
		uint32_t r_crc,
		uint32_t g_crc,
		uint32_t b_crc,
		uint32_t frame_count,
		int32_t x_start,
		int32_t y_start,
		int32_t x_end,
		int32_t y_end)
{
	struct crtc *crtc = (struct crtc *)data;

	if (crtc->b_crc) {
		crtc->src_crc.crc_r = r_crc;
		crtc->src_crc.crc_g = g_crc;
		crtc->src_crc.crc_b = b_crc;
		printf("new roi x_start: %d, y_start: %d, x_end: %d, y_end: %d.\n"
			"r_crc: %#x, g_crc: %#x, b_crc: %#x,\n"
			"frame_count: %d.\n",
			x_start, y_start, x_end, y_end,
			r_crc, g_crc, b_crc,
			frame_count);
		crtc->b_crc = false;
	} else if ((r_crc == crtc->src_crc.crc_r) &&
		   (g_crc == crtc->src_crc.crc_g) &&
		   (b_crc == crtc->src_crc.crc_b)) {
		printf("display frame %d is secure.\n"
			"roi x_start: %d, y_start: %d, x_end: %d, y_end: %d.\n"
			"frame r_crc: %#x, g_crc: %#x, b_crc: %#x.\n",
			frame_count,
			x_start, y_start, x_end, y_end,
			r_crc, g_crc, b_crc);
	} else {
		printf("display frame %d is insecure.\n"
			"roi x_start: %d, y_start: %d, x_end: %d, y_end: %d.\n"
			"correct r_crc: %#x, g_crc: %#x, b_crc: %#x.\n"
			"frame r_crc: %#x, g_crc: %#x, b_crc: %#x.\n",
			frame_count,
			x_start, y_start, x_end, y_end,
			crtc->src_crc.crc_r,
			crtc->src_crc.crc_g,
			crtc->src_crc.crc_b,
			r_crc, g_crc, b_crc);
	}
}

static void
crc_done(void *data,
	 struct secure_display_crtc_v1 *secure_display_crtc_v1)
{
	printf("secure display crc data is ready\n");
}

static const struct secure_display_crtc_v1_listener display_crtc_listener = {
	.rgb_crc = compare_rgb_crc,
	.crtc_out_fence = noop,
	.done = crc_done,
};

static void
registry_global(void *data,
		struct wl_registry *wl_registry,
		uint32_t name,
		const char *interface,
		uint32_t version)
{
	struct secure_display *state = data;

	if (strcmp(interface, "wl_compositor") == 0) {
		printf("interface %s detected\n", interface);
		state->compositor = wl_registry_bind(wl_registry,
					 name, &wl_compositor_interface, 1);
	} else if (strcmp(interface, "xdg_wm_base") == 0) {
		printf("interface %s detected\n", interface);
		state->wm_base = wl_registry_bind(wl_registry, name,
					      &xdg_wm_base_interface, 1);
	} else if (strcmp(interface, "wl_output") == 0) {
		printf("interface %s detected\n", interface);
		add_output(state, wl_registry, name);
	} else if (strcmp(interface, zxdg_output_manager_v1_interface.name) == 0) {
		printf("interface %s detected\n", interface);
		state->xdg_output_manager =
			wl_registry_bind(wl_registry, name,
					&zxdg_output_manager_v1_interface, 2);
	} else if (strcmp(interface, secure_display_manager_v1_interface.name) == 0) {
		printf("interface %s detected\n", interface);
		state->secure_manager = wl_registry_bind(wl_registry,
				name, &secure_display_manager_v1_interface, 1);
	}
}

static void
registry_global_remove(void *data,
		       struct wl_registry *registry,
		       uint32_t name) {
}

static const
struct wl_registry_listener registry_listener = {
	.global = registry_global,
	.global_remove = registry_global_remove,
};

static void
print_usage(void)
{
	static const char usage[] =
		"Usage: secure-display [OPTIONS] ...\n"
		"Set secure roi to a select display output.\n"
		"\n"
		"  --output=name     select output to display secure roi\n"
		"                    output name can check with weston-info\n"
		"  --x_start=%d      start x position of roi\n"
		"  --y_start=%d      start y position of roi\n"
		"  --x_end=%d        end x position of roi\n"
		"  --y_end=%d        end y position of roi\n"
		"  --color=%c        draw gles rectangle in color\n"
		"  --help, -h        print help message and quit\n"
		"\n"
		"Usage:\n"
		"weston-simple-secure-display --output=DP-1 --x_start=480 --y_start=270 --x_end=960 --y_end=540 --color='R'\n";
	fprintf(stderr, "%s", usage);

	exit(0);
}

static void
signal_term(int signum)
{
	b_running = false;
}

static void
signal_update(int signum)
{
	b_setroi = true;
}

int
main(int argc, char **argv)
{
	struct secure_display display = { 0 };
	struct crtc crtc = { 0 };
	struct window  window  = { 0 };
	int32_t x_start, y_start, x_end, y_end;
	char connector_name[10] = {'\0'};
	char c = '\0';
	struct output *secure_output = NULL;
	struct wl_output *wl_output = NULL;
	struct sigaction sigint;
	struct sigaction sigtstp;
	struct wl_registry *registry;
	int ret = 0;

	if (argc == 1) {
		print_usage();
	}

	for (int i = 1; i < argc; ++i) {
		if (strcmp(argv[i], "--help") == 0 ||
		    strcmp(argv[i], "-h") == 0) {
			print_usage();
		} else if (sscanf(argv[i], "--output=%s", connector_name) > 0) {
			printf("output: %s\n", connector_name);
			continue;
		} else if (sscanf(argv[i], "--x_start=%d", &x_start) > 0) {
			printf("input x_start: %d\n", x_start);
			continue;
		} else if (sscanf(argv[i], "--y_start=%d", &y_start) > 0) {
			printf("input y_start: %d\n", y_start);
			continue;
		} else if (sscanf(argv[i], "--x_end=%d", &x_end) > 0) {
			printf("input x_end: %d\n", x_end);
			continue;
		} else if (sscanf(argv[i], "--y_end=%d", &y_end) > 0) {
			printf("input y_end: %d\n", y_end);
			continue;
		} else if (sscanf(argv[i], "--color=%c", &c) > 0) {
			printf("draw gles rectangle in color: %c\n", c);
			window.gles.c = c;
			continue;
		} else {
			printf("Invalid option: %s\n", argv[i]);
			exit(0);
		}
	}

	wl_list_init(&display.outputs);
	wl_list_init(&display.crtcs);

	display.display = wl_display_connect(NULL);
	assert(display.display);

	registry = wl_display_get_registry(display.display);
	wl_registry_add_listener(registry, &registry_listener, &display);
	wl_display_dispatch(display.display);
	wl_display_roundtrip(display.display);

	assert(display.secure_manager);

	wl_list_for_each(secure_output, &display.outputs, output_link) {
		if ((strlen(secure_output->name) != 0) &&
			(strcmp(secure_output->name, connector_name) == 0)) {
			wl_output = secure_output->output;
			break;
		}
	}
	assert(secure_output);
	assert(wl_output);

	window.display = &display;
	window.output = secure_output;

	init_egl(&display);
	init_surface(&window);

	crtc.secure_crtc =
		secure_display_manager_v1_create_secure_output(
				display.secure_manager, wl_output);

	assert(crtc.secure_crtc);
	crtc.display = &display;
	crtc.b_roi = true;
	crtc.roi.x_start = x_start;
	crtc.roi.y_start = y_start;
	crtc.roi.x_end = x_end;
	crtc.roi.y_end = y_end;
	secure_output->crtc = &crtc;

	wl_list_insert(&display.crtcs, &crtc.crtc_link);

	secure_display_crtc_v1_add_listener(crtc.secure_crtc,
			      &display_crtc_listener,
			      &crtc);

	/*
	 * exit this application on SIGINT, press ctrl+c to generate SIGINT
	 */
	sigint.sa_handler = signal_term;
	sigemptyset(&sigint.sa_mask);
	sigint.sa_flags = SA_RESETHAND;
	sigaction(SIGINT, &sigint, NULL);

	/*
	 * update roi on SIGTSTP, press ctrl+z to generate SIGTSTP
	 */
	sigtstp.sa_handler = signal_update;
	sigemptyset(&sigtstp.sa_mask);
	sigtstp.sa_flags = SA_RESTART;
	sigaction(SIGTSTP, &sigtstp, NULL);

	while (b_running && window.wait_for_configure) {
		if (window.wait_for_configure)
			continue;

		assert(window.toplevel_fullscreen);
	}

	init_gl(&window);
	redraw(&window, NULL, 0);

	while (b_running && ret != -1) {
		if (b_setroi) {
			printf("please enter roi x_start: y_start: x_end: y_end: \n");
			scanf("%d %d %d %d", &x_start, &y_start, &x_end, &y_end);
			printf("new roi x_start:%d y_start:%d x_end:%d y_end:%d\n",
				x_start,y_start,x_end, y_end);

			crtc.roi.x_start = x_start;
			crtc.roi.y_start = y_start;
			crtc.roi.x_end = x_end;
			crtc.roi.y_end = y_end;

			crtc.b_roi = true;
			b_setroi = false;
		}

		ret = wl_display_roundtrip(display.display);
	}

	printf("exit secure-display\n");

	if (window.gles.program)
		glDeleteProgram(window.gles.program);

	if (window.callback)
		wl_callback_destroy(window.callback);

	/* send all pending commands to the server, wait it completes
	 * these requests and acknowledges client, then send drm command
	 * to disable secure display function.
	 */
	wl_display_dispatch(display.display);
	wl_display_roundtrip(display.display);

	sleep(1);

	secure_display_crtc_v1_set_roi(crtc.secure_crtc, 0, 0, 0, 0, 0);
	wl_display_roundtrip(display.display);

	eglMakeCurrent(display.egl.dpy,
		       EGL_NO_SURFACE, EGL_NO_SURFACE,
		       EGL_NO_CONTEXT);

	weston_platform_destroy_egl_surface(display.egl.dpy,
					    window.egl_surface);
	wl_egl_window_destroy(window.native);

	if (window.xdg_toplevel)
		xdg_toplevel_destroy(window.xdg_toplevel);
	if (window.xdg_surface)
		xdg_surface_destroy(window.xdg_surface);
	wl_surface_destroy(window.surface);

	if (display.egl.context != EGL_NO_CONTEXT)
		eglDestroyContext(display.egl.dpy, display.egl.context);
	eglTerminate(display.egl.dpy);
	eglReleaseThread();

	if (display.wm_base)
		xdg_wm_base_destroy(display.wm_base);

	if (display.compositor)
		wl_compositor_destroy(display.compositor);

	if (registry)
		wl_registry_destroy(registry);

	wl_display_disconnect(display.display);

	return 0;
}
